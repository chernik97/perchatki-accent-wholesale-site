package ru.perchatki.accent.wholesale.site;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@ComponentScan("ru.perchatki.accent.wholesale.site")
public class ServiceRunner {
    public static void main(String[] args) {
        run(ServiceRunner.class, args);
    }
}
