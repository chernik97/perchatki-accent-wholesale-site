package ru.perchatki.accent.wholesale.site.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import ru.perchatki.accent.wholesale.site.exception.MessagingExceptionRuntime;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.List;

import static java.lang.String.format;
import static javax.mail.Message.RecipientType.CC;
import static javax.mail.Part.INLINE;
import static javax.mail.internet.MimeMessage.RecipientType.TO;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import static org.apache.commons.lang.CharEncoding.UTF_8;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;
import static org.springframework.util.ResourceUtils.getURL;

@Component
public class MailingSenderImpl implements MailingSender {

    private final Logger logger = getLogger(MailingSenderImpl.class);

    static final String HTML_CONTENT_TYPE = "text/html; charset=utf-8";

    private static final String ACCENT_GLOVES_CID = "<accent-gloves>";
    private static final String SIGNATURE_IMG_PATH = "templates/email/img/accent-gloves.png";

    private File signatureImageFile;

    @Autowired
    private JavaMailSender javaMailSender;

    public MailingSenderImpl() {
        try {

            signatureImageFile = new File(SIGNATURE_IMG_PATH);

            //prevent problem with signature image loading  from .jar file
            final InputStream signatureImageInputStream = getURL(CLASSPATH_URL_PREFIX + SIGNATURE_IMG_PATH).openStream();

            copyInputStreamToFile(signatureImageInputStream, signatureImageFile);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void publishEmail(final String recipient, final String subject, final String emailBody) {

        logger.info("Sending message to {} with subject {}", recipient, subject);

        final MimeMessage mail = javaMailSender.createMimeMessage();

        try {
            mail.addRecipient(TO, new InternetAddress(recipient));
            sendEmail(mail, subject, emailBody);
        } catch (MessagingException e) {
            logger.error(format("Invalid email address=%s", recipient), e);
            throw new MessagingExceptionRuntime(format("Invalid email address=%s", recipient), e);
        }

    }

    @Override
    public void publishEmail(final String recipient, final List<String> ccRecipients, final String subject,
            final String emailBody) {
        logger.info("Sending message to {} with cc {} with subject {}", recipient, ccRecipients, subject);

        final MimeMessage mail = javaMailSender.createMimeMessage();

        try {
            mail.addRecipient(TO, new InternetAddress(recipient));

            for (final String ccRecipient : ccRecipients) {
                mail.addRecipient(CC, new InternetAddress(ccRecipient));
            }

            sendEmail(mail, subject, emailBody);
        } catch (MessagingException e) {
            logger.error(format("Invalid email address=%s", recipient), e);
            throw new MessagingExceptionRuntime(format("Invalid email address=%s", recipient), e);
        }
    }

    private void sendEmail(final MimeMessage mail, final String subject, final String emailBody) {
        try {
            mail.setSubject(subject, UTF_8);

            final MimeMultipart multipart = new MimeMultipart("related");

            final BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(emailBody, HTML_CONTENT_TYPE);

            multipart.addBodyPart(messageBodyPart);

            final MimeBodyPart signatureImage = new MimeBodyPart();

            signatureImage.attachFile(signatureImageFile);
            signatureImage.setContentID(ACCENT_GLOVES_CID);
            signatureImage.setDisposition(INLINE);

            multipart.addBodyPart(signatureImage);

            mail.setContent(multipart);

            javaMailSender.send(mail);

            logger.info("Mail was sent");
        } catch (MessagingException e) {
            logger.error("Failed to prepare email", e);
            throw new MessagingExceptionRuntime("Failed to prepare email", e);
        } catch (IOException e) {
            logger.error("Failed to attach file", e);
            throw new UncheckedIOException(e);
        }
    }
}
