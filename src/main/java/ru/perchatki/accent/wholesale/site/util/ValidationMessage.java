package ru.perchatki.accent.wholesale.site.util;


public final class ValidationMessage {
    private ValidationMessage() {
    }

    public static final String BLANK_FIELD = "Введите требуемое поле";
    public static final String MAX_FIELD_SIZE = "Длина превышена";
    public static final String HTML_INJECTION = "Может содержать небезопасный html контент";
}
