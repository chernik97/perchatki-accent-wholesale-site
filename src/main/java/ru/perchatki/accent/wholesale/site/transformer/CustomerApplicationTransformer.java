package ru.perchatki.accent.wholesale.site.transformer;


import ru.perchatki.accent.wholesale.site.dto.CustomerApplication;
import org.apache.commons.collections.map.HashedMap;

import java.util.Map;

public class CustomerApplicationTransformer {

    private static final String VELOCITY_NAME = "name";
    private static final String VELOCITY_CITY = "city";
    private static final String VELOCITY_PHONE_NUMBER = "phoneNumber";
    private static final String VELOCITY_EMAIL = "email";
    private static final String VELOCITY_RETAIL_OUTLET_TYPE = "retailOutletType";
    private static final String VELOCITY_SPECIALIZATION = "specialization";
    private static final String VELOCITY_SUITABLE_TIME_FROM = "suitableTimeFrom";
    private static final String VELOCITY_SUITABLE_TIME_TO = "suitableTimeTo";

    public static Map<String, Object> transformCustomerApplication(final CustomerApplication customerApplication) {
        final Map<String, Object> customerApplicationMap = new HashedMap();

        customerApplicationMap.put(VELOCITY_NAME, customerApplication.getName());
        customerApplicationMap.put(VELOCITY_CITY, customerApplication.getCity());
        customerApplicationMap.put(VELOCITY_EMAIL, customerApplication.getEmail());
        customerApplicationMap.put(VELOCITY_PHONE_NUMBER, customerApplication.getPhoneNumber());
        customerApplicationMap.put(VELOCITY_RETAIL_OUTLET_TYPE, customerApplication.getRetailOutletType().getDisplayName());
        customerApplicationMap.put(VELOCITY_SPECIALIZATION, customerApplication.getSpecialization().getDisplayName());
        customerApplicationMap.put(VELOCITY_SUITABLE_TIME_FROM, customerApplication.getSuitableTimeFrom());
        customerApplicationMap.put(VELOCITY_SUITABLE_TIME_TO, customerApplication.getSuitableTimeTo());

        return customerApplicationMap;
    }
}
