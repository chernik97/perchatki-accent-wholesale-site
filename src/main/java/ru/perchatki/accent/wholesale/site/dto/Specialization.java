package ru.perchatki.accent.wholesale.site.dto;


public enum Specialization {
    NOT_FILLED("Не указано"),
    ACCESSORIES("Акссесуары"),
    GLOVES("Перчатки"),
    WOMEN_HANDBAGS("Кожаные сумки"),
    FOOTWEAR("Обувь"),
    OUTERWEAR("Верхняя одежда"),
    FUR_CLOTHES("Одежда из меха");

    private final String displayName;

    Specialization(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
