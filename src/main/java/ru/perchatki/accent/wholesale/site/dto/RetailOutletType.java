package ru.perchatki.accent.wholesale.site.dto;


public enum RetailOutletType {
    NOT_FILLED("Не указано"),
    MALL_ISLAND("Остров в торговом центре"),
    MALL_DIVISION("Отдел в торговом центре"),
    STORE("Магазин"),
    CHAIN_STORES("Сеть магазинов");

    private final String displayName;

    RetailOutletType(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
