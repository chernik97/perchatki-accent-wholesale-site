package ru.perchatki.accent.wholesale.site.config;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.tuckey.web.filters.urlrewrite.Conf;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;
import org.tuckey.web.filters.urlrewrite.UrlRewriter;

import javax.servlet.*;
import java.io.InputStream;

@Component
public class UrlRewriteFilterConfig extends UrlRewriteFilter {
    private static final String URL_REWRITE_XML_FILE = "urlrewrite.xml";

    private UrlRewriter urlRewriter;

    @Override
    public void loadUrlRewriter(FilterConfig filterConfig) throws ServletException {
        try {
            InputStream inputStream = new ClassPathResource(URL_REWRITE_XML_FILE).getInputStream();
            Conf conf = new Conf(filterConfig.getServletContext(), inputStream, URL_REWRITE_XML_FILE, "");
            urlRewriter = new UrlRewriter(conf);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    public UrlRewriter getUrlRewriter(ServletRequest request, ServletResponse response, FilterChain chain) {
        return urlRewriter;
    }

    @Override
    public void destroyUrlRewriter() {
        if (urlRewriter != null)
            urlRewriter.destroy();
    }
}