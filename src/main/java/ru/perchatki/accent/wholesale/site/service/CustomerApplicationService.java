package ru.perchatki.accent.wholesale.site.service;

import ru.perchatki.accent.wholesale.site.dto.CustomerApplication;

public interface CustomerApplicationService {
    void processCustomerApplication(CustomerApplication customerApplication);
}
