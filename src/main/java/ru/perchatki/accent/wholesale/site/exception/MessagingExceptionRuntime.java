package ru.perchatki.accent.wholesale.site.exception;

public class MessagingExceptionRuntime extends RuntimeException {

    public MessagingExceptionRuntime() {
        super();
    }

    public MessagingExceptionRuntime(String message) {
        super(message);
    }

    public MessagingExceptionRuntime(String message, Throwable cause) {
        super(message, cause);
    }

    public MessagingExceptionRuntime(Throwable cause) {
        super(cause);
    }

    protected MessagingExceptionRuntime(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
