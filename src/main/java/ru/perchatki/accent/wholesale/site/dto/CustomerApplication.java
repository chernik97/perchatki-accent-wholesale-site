package ru.perchatki.accent.wholesale.site.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import ru.perchatki.accent.wholesale.site.util.ValidationMessage;

import javax.validation.constraints.Size;
import java.io.Serializable;

import static org.apache.commons.lang.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.StringUtils.EMPTY;

public class CustomerApplication implements Serializable {

    private static final long serialVersionUID = -8582553475226281591L;

    @NotBlank(message = ValidationMessage.BLANK_FIELD)
    @Size(max = 50, message = ValidationMessage.MAX_FIELD_SIZE)
    @SafeHtml(message = ValidationMessage.HTML_INJECTION)
    private String name;

    @NotBlank(message = ValidationMessage.BLANK_FIELD)
    @Size(max = 30, message = ValidationMessage.MAX_FIELD_SIZE)
    @SafeHtml(message = ValidationMessage.HTML_INJECTION)
    private String phoneNumber;

    @NotBlank(message = ValidationMessage.BLANK_FIELD)
    @Email
    private String email;

    @NotBlank(message = ValidationMessage.BLANK_FIELD)
    @SafeHtml(message = ValidationMessage.HTML_INJECTION)
    @Size(max = 50, message = ValidationMessage.MAX_FIELD_SIZE)
    private String city;

    private Specialization specialization;

    private RetailOutletType retailOutletType;

    //formatted in setter
    private String suitableTimeFrom;

    //formatted in setter
    private String suitableTimeTo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public RetailOutletType getRetailOutletType() {
        return retailOutletType;
    }

    public void setRetailOutletType(RetailOutletType retailOutletType) {
        this.retailOutletType = retailOutletType;
    }

    public String getSuitableTimeFrom() {
        return suitableTimeFrom;
    }

    public void setSuitableTimeFrom(String suitableTimeFrom) {
        this.suitableTimeFrom = formatSuitableTime(suitableTimeFrom);
    }

    public String getSuitableTimeTo() {
        return suitableTimeTo;
    }

    public void setSuitableTimeTo(String suitableTimeTo) {
        this.suitableTimeTo = formatSuitableTime(suitableTimeTo);
    }

    @Override
    public boolean equals(Object o) {
        return reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return reflectionToString(this);
    }

    private String formatSuitableTime(final String suitableTime) {
        if("not_filled".equals(suitableTime)) return EMPTY;
        return suitableTime.replace("_", ":");
    }

}
