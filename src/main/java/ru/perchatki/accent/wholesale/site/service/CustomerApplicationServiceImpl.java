package ru.perchatki.accent.wholesale.site.service;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.perchatki.accent.wholesale.site.dto.CustomerApplication;
import ru.perchatki.accent.wholesale.site.transformer.CustomerApplicationTransformer;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang.CharEncoding.UTF_8;
import static org.springframework.ui.velocity.VelocityEngineUtils.mergeTemplateIntoString;

@Component
public class CustomerApplicationServiceImpl implements CustomerApplicationService {

    private static final String ADMIN_EMAIL_TEMPLATE_FILE = "/templates/email/admin_application_email.vm";
    private static final String CUSTOMER_EMAIL_TEMPLATE_FILE = "/templates/email/customer_application_email.vm";

    private static final DateTimeFormatter formatter = ofPattern("yyyy-MM-dd HH:mm:ss");

    private static final String ADMIN_EMAIL_SUBJECT = "Поступление заявки - %s";
    private static final String CUSTOMER_EMAIL_SUBJECT = "Ваша зявка получена";

    private final String administratorEmail;
    private final List<String> administratorCCEmails;

    private final MailingSender mailingService;
    private final VelocityEngine velocityEngine;

    @Autowired public CustomerApplicationServiceImpl(final MailingSender mailingService,
            @Value("${administrator.email}") final String administratorEmail,
            @Value("#{'${administrator.cc.email}'.split(',')}") final List<String> administratorCCEmails,
            final VelocityEngine velocityEngine) {
        this.mailingService = mailingService;
        this.administratorEmail = administratorEmail;
        this.administratorCCEmails = administratorCCEmails;
        this.velocityEngine = velocityEngine;
    }

    public void processCustomerApplication(final CustomerApplication customerApplication) {

        final Map model = CustomerApplicationTransformer.transformCustomerApplication(customerApplication);

        publishEmailToCustomer(customerApplication.getEmail(), model);
        publishEmailToAdministrator(administratorEmail, administratorCCEmails, model);
    }

    private void publishEmailToCustomer(final String recipient, final Map model) {
        final String emailBody = generateEmailBody(CUSTOMER_EMAIL_TEMPLATE_FILE, model);

        mailingService.publishEmail(recipient, CUSTOMER_EMAIL_SUBJECT, emailBody);
    }

    private void publishEmailToAdministrator(final String recipient, final List<String> ccRecipients, final Map model) {
        final String emailBody = generateEmailBody(ADMIN_EMAIL_TEMPLATE_FILE, model);

        final String adminEmailSubject = format(ADMIN_EMAIL_SUBJECT, now().format(formatter));

        mailingService.publishEmail(recipient, ccRecipients, adminEmailSubject, emailBody);
    }

    private String generateEmailBody(final String templateLocation, final Map<String, Object> model) {

        return mergeTemplateIntoString(velocityEngine, templateLocation, UTF_8, model);
    }


}
