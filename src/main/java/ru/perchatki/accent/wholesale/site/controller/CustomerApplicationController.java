package ru.perchatki.accent.wholesale.site.controller;

import ru.perchatki.accent.wholesale.site.dto.CustomerApplication;
import ru.perchatki.accent.wholesale.site.service.CustomerApplicationService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class CustomerApplicationController {
    private final Logger logger = getLogger(CustomerApplicationController.class);

    private static final String CUSTOMER_APPLICATION = "customerApplication";
    private static final String IS_SUCCESSFUL_SENT = "isSuccessfulSent";

    private final CustomerApplicationService customerApplicationService;

    @Autowired
    public CustomerApplicationController(final CustomerApplicationService customerApplicationService) {
        this.customerApplicationService = customerApplicationService;
    }

    @RequestMapping(value = "/application", method = GET)
    public String getApplicationPage(final Model model) {
        model.addAttribute(CUSTOMER_APPLICATION, new CustomerApplication());

        return "application";
    }

    @RequestMapping(value = "/application", method = POST)
    public String publishCustomerApplication(@Valid @ModelAttribute final CustomerApplication customerApplication,
                                             final BindingResult errors, final Model model) {
        logger.info("Receive customer application={}", customerApplication);

        if (!errors.hasErrors()) {

            customerApplicationService.processCustomerApplication(customerApplication);

            model.addAttribute(CUSTOMER_APPLICATION, new CustomerApplication());
            model.addAttribute(IS_SUCCESSFUL_SENT, true);

        }

        return "application";
    }
}
