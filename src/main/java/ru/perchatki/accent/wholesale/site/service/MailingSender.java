package ru.perchatki.accent.wholesale.site.service;

import java.util.List;

public interface MailingSender {
    void publishEmail(String recipient, String subject, String emailBody);

    void publishEmail(String recipient, List<String> ccRecipient, String subject, String emailBody);
}
