package ru.perchatki.accent.wholesale.site.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DefaultController {
    @GetMapping("/")
    public String home() {
        return "index";
    }

    @GetMapping("/catalog")
    public String admin() {
        return "catalog";
    }

    @GetMapping("/company")
    public String company() {
        return "company";
    }

    @GetMapping("/contacts")
    public String contacts() {
        return "contacts";
    }

    @GetMapping("/retail_customers")
    public String retailCustomers() {
        return "retail_customers";
    }

    @GetMapping("/wholesale_customers")
    public String wholesaleCustomers() {
        return "wholesale_customers";
    }

    @GetMapping("/useful")
    public String useful() {
        return "useful";
    }
}
