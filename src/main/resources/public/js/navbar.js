$(document).ready(function () {

    var list = [];

    function preloadImages(array) {
        for (var i = 0; i < array.length; i++) {
            var img = new Image();
            img.onload = function () {
                var index = list.indexOf(this);
                if (index !== -1) {
                    // remove image from the array once it's loaded
                    // for memory consumption reasons
                    list.splice(index, 1);
                }
            }
            list.push(img);
            img.src = array[i];
        }
    }

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });


    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 54
    });

    preloadImages(["img/logo-label.png", "img/logo-whole-label.png"]);

    // Collapse the navbar when page is scrolled
    $(window).scroll(function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");

            $(".logo-image").attr("src", "./img/logo-label.png").addClass("logo-scrolled");

        } else {
            $("#mainNav").removeClass("navbar-shrink");

            $(".logo-image").removeClass("logo-scrolled");
            $(".logo-image").attr("src", "./img/logo-whole-label.png");
        }
    });
});