$(document).ready(function () {
    $("#personal-data").click(function () {
        $("#submit").attr("disabled", !this.checked);
    });

    //prevent any submissions of form
    $("#applicationForm").submit(function () {
        $("#submit").attr("disabled", true);
    });
});