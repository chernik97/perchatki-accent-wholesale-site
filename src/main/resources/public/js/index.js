$(document).ready(function () {
    var imgPath = 'img/slides/';


    $(".intro-header").bgswitcher({
        images: [imgPath + 'slide3.jpg', imgPath + 'slide6.jpg',
            imgPath + 'slide4.jpg']
    });


    $('.multiple-items').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});