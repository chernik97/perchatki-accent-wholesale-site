package ru.perchatki.accent.wholesale.site.service;


import ru.perchatki.accent.wholesale.site.exception.MessagingExceptionRuntime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import static java.util.Arrays.asList;
import static javax.mail.Message.RecipientType.CC;
import static javax.mail.Message.RecipientType.TO;
import static org.apache.commons.lang.CharEncoding.UTF_8;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class MailingSenderTest {

    private static final String TEST_RECIPIENT = "test@gmail.com";
    private static final String TEST_CC_RECIPIENT_FIRST = "testCCFisrt@gmail.com";
    private static final String TEST_CC_RECIPIENT_SECOND = "testCCSecond@gmail.com";
    private static final String TEST_INVALID_RECIPIENT = "t est@g mail. com";
    private static final String TEST_SUBJECT = "test subject";
    private static final String TEST_EMAIL_BODY = "test email body";

    @Mock
    private MimeMessage mimeMessage;

    @Mock
    private JavaMailSender javaMailSender;

    @InjectMocks
    private MailingSender mailingSender = new MailingSenderImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_publishEmailWithoutCC_success() throws MessagingException {
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        mailingSender.publishEmail(TEST_RECIPIENT, TEST_SUBJECT, TEST_EMAIL_BODY);

        verifyCommonStepsWhenPublishEmail();
    }

    @Test
    public void test_publishEmailWithCC_success() throws MessagingException {

        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        mailingSender.publishEmail(TEST_RECIPIENT, asList(TEST_CC_RECIPIENT_FIRST, TEST_CC_RECIPIENT_SECOND), TEST_SUBJECT, TEST_EMAIL_BODY);

        verify(mimeMessage).addRecipient(CC, new InternetAddress(TEST_CC_RECIPIENT_FIRST));
        verify(mimeMessage).addRecipient(CC, new InternetAddress(TEST_CC_RECIPIENT_SECOND));
        verifyCommonStepsWhenPublishEmail();
    }

    private void verifyCommonStepsWhenPublishEmail() throws MessagingException {
        verify(mimeMessage).addRecipient(TO, new InternetAddress(TEST_RECIPIENT));
        verify(mimeMessage).setSubject(TEST_SUBJECT, UTF_8);
        verify(mimeMessage).setContent(any(MimeMultipart.class));

        verify(javaMailSender).createMimeMessage();
        verify(javaMailSender).send(mimeMessage);

        verifyNoMoreInteractions(mimeMessage);
        verifyNoMoreInteractions(javaMailSender);
    }

    @Test(expected = MessagingExceptionRuntime.class)
    public void test_publishEmail_failed() throws MessagingException {
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        mailingSender.publishEmail(TEST_INVALID_RECIPIENT, TEST_SUBJECT, TEST_EMAIL_BODY);
    }
}
