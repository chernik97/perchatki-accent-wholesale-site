# Accent Gloves whole sale site

### Deployment JVM options to be specified:

```
 -Duser.timezone=Europe/Moscow 

```

### How to build .jar

```
 mvn clean install

```

### How to run application

```
mvn spring-boot:run
```

Link: https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html


### How to build ssl certificate

1. Go to site [www.sslforfree.com](https://www.sslforfree.com)

2. Follow instructions to renew certificate with own csr in **src/main/resources/ssl/generated/perchatki-accent.csr** 
and download the files to generate certificate.

3. Run on linux: 
with own csr
```bash
openssl pkcs12 -export -in certificate.crt -inkey perchatki-accent.key -out perchatki-accent.p12 -name tomcat -CAfile ca_bundle.crt -caname root
```
without own csr
```bash
openssl pkcs12 -export -in certificate.crt -inkey private.key -out perchatki-accent.p12 -name tomcat -CAfile ca_bundle.crt -caname root
```
4. Run on linux: 
```bash
keytool -importkeystore \
   -deststorepass asdfghjkl_1996 -destkeypass asdfghjkl_1996 -destkeystore perchatki-accent.keystore \
   -srckeystore perchatki-accent.p12 -srcstoretype PKCS12 -srcstorepass asdfghjkl_1996 \
   -alias tomcat
```